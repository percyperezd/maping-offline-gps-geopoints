package com.group.yali.gpsdatacollector;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;


import org.osmdroid.api.IMapController;
import org.osmdroid.api.Polyline;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.List;


/**
 *   This trivial code get GPS data location and plot them on a offline map using OSMdroid.
 */
public class MainActivity extends Activity{

    /**
     *      GPS android variables
     */
    LocationManager locationManager;
    TextView messages, general;
    Coords  coords;
    Double gpsLatitude=0.0, gpsLongitude=0.0;

    /**
     *      OSMDroid variables
     */
    MapView mapView = null;
    GeoPoint    startGeoPoint = null;
    Marker startMarker = null;
    Polyline polyline =null;
    List<GeoPoint> pointsToDraw;
    IMapController iMapController;
    Drawable mapIcon;
    ItemizedIconOverlay<OverlayItem> items;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         *    For GPS data location
         */
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        messages = (TextView) findViewById(R.id.messages);
        general = (TextView) findViewById(R.id.general);


        /**
         *      For set and display the offline tiles manually generated with MOBAC
         */
        mapView = (MapView) findViewById(R.id.mapView);
        // This is to read the offline tiles from /sdcard/osndroid/MapquestOSM.zip
        mapView.setTileSource(TileSourceFactory.MAPQUESTOSM);
        mapView.setBuiltInZoomControls(true);
        mapView.setMultiTouchControls(true);

        iMapController = mapView.getController();
        // The offline tile loaded in /sdcard/osmdroid should contain the zoom 15
        iMapController.setZoom(15);
        iMapController.setCenter(new GeoPoint(56.34104, -2.811547));

    }

    protected void onResume() {
        super.onResume();
        coords = new Coords();
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, coords);
        } catch (SecurityException e) {
            messages.setText("ERROR " + e);
        }
        updateLocation();
    }

    protected void updateLocation(){

        messages.setText("LAT,LON " + gpsLatitude + "," + gpsLongitude);
//        startGeoPoint = new GeoPoint(56.34104, -2.811547);  // data location in St Andrews
        startGeoPoint = new GeoPoint(gpsLatitude, gpsLongitude);
        startMarker = new Marker(mapView);
        startMarker.setPosition(startGeoPoint);
        startMarker.setTitle("Lat,Lon: " + gpsLongitude + "," + gpsLatitude);
//        mapView.getOverlays().clear();  // Clear markers
        mapView.getOverlays().add(startMarker);
        mapView.invalidate();

        /**
         *      TODO:  Change the marker, create routes, and identify multiple nodes by colors.
         */
    }

    protected void onPause(){
        super.onPause();
        try{
            locationManager.removeUpdates(coords);
        }catch (SecurityException e){
            messages.setText("ERROR "+ e);
        }
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        System.exit(0);
    }

    private class Coords implements LocationListener{


        @Override
        public void onLocationChanged(Location location) {
            messages.setText("\n Lat: " + location.getLatitude() + " , Long: " + location.getLongitude());
            general.setText("\n Provider " + location.getProvider() + " accuracy " + location.getAccuracy()
                    + "\n speed " + location.getSpeed() + ", " + location.getTime()
                    + "\n bearing " + location.getBearing() + "\n");
            gpsLatitude = location.getLatitude();
            gpsLongitude = location.getLongitude();
            updateLocation();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    }
}
